package menuDeOpciones;

import java.util.Scanner;

public class Cinematografica {

    private int entrada;

    public void entrada(){
        Scanner sc = new Scanner(System.in);
        setEntrada(sc.nextInt());
    }

    public void menu(){
        do {

            System.out.println("****** GESTIÓN CINEMATOGRÁFICA ******");
            System.out.println(" 1. NUEVO ACTOR \n 2. BUSCAR ACTOR \n 3. ELIMINAR ACTOR \n 4. MODIFICAR ACTOR \n 5. VER TODOS LOS ACTORES \n 6. VER PELICULAS DE LOS ACTORES \n 7. VER CATEGORIA DE LAS PELICULAS DE LOS ACTORES \n" +
                    " 8. SALIR");
            entrada();
            if (getEntrada()>8 || getEntrada()<1){
                System.out.println("Ingrese una opcion correcta");

            }
        }while (getEntrada()!=8);
    }


    public int getEntrada() {
        return entrada;
    }

    public void setEntrada(int entrada) {
        this.entrada = entrada;
    }
}

