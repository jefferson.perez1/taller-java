package doWhile;

import java.util.Scanner;

public class Numero {

    public void comparador() {
        double entrada;
        do {
            System.out.println("se comprobará si tu numero es mayor o igual que 0");
            System.out.println("Ingresa numero");

            Scanner sc = new Scanner(System.in);
            entrada = sc.nextDouble();

            if(entrada<0) {
                System.out.println("El numero es menor que 0");
            }
            else if (entrada>0){
                System.out.println("El numero es mayor que 0");
            }
            else {
                System.out.println("El numero es igual que 0");
            }

        }
        while (entrada < 0);

    }
}
