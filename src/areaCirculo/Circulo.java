package areaCirculo;

import java.util.Scanner;

public class Circulo {
    private Double radio;


    public void ingresoRadio(){
        System.out.print(" Ingrese el Radio: ");
        Scanner radio = new Scanner(System.in);
        setRadio(radio.nextDouble());
    }
    public double operacionArea(){
        ingresoRadio();
        Double area= Math.pow(getRadio(),2)*Math.PI;
        return area;
    }
    public void calcularAreaCirculo(){
        System.out.println(" El Area del circulo Es: "+operacionArea());
    }

    public Double getRadio() {
        return radio;
    }

    public void setRadio(Double radio) {
        this.radio = radio;
    }
}
