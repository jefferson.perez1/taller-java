package fechaYHora;


import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Fecha {

    public void fechaYHora () {
        LocalDate hoy = LocalDate.now();

        Calendar calendario = new GregorianCalendar();
        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int minutos = calendario.get(Calendar.MINUTE);
        int segundos = calendario.get(Calendar.SECOND);



        System.out.println(hoy + " HH "+ hora+" MM "+minutos+" SS "+segundos);
    }
}
