package serie;

public class VideoJuego implements IEntregable {

    private  String titulo;
    private int horasEstimadas = 10;
    private boolean entregado = false;
    private String genero;
    private String compañia;


    public VideoJuego(){}

    public VideoJuego(String titulo, int horasEstimadas) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
    }

    public VideoJuego(String titulo, int horasEstimadas, String genero, String compañia) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
        this.genero = genero;
        this.compañia = compañia;
    }

    @Override
    public String toString(){
        return "Titulo del videojuego: "+ getTitulo() +"\n" + "Numero de Horas Estimadas. " + getHorasEstimadas()+ "\n" +
                "Genero del Videojuego: " +getGenero()+"\n" +"Compañia creadora del videojuego: "+getCompañia();
    }

    @Override
    public void entregar() {
        this.entregado = true;

    }

    @Override
    public void devolver() {
        this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return this.entregado;
    }



    public int compareTo(VideoJuego videoJuego) {
        if (this.horasEstimadas > videoJuego.getHorasEstimadas()) {
            return this.horasEstimadas;
        } else {
            return videoJuego.getHorasEstimadas();
        }
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(int horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }


}
