package serie;

public interface IEntregable {
    public void entregar();
    public  void devolver();
    public boolean isEntregado();
}
