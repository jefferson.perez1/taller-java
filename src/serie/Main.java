package serie;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {



        Serie dark =new Serie("Dark",2,"Suspenso","Netflix");
        Serie starGate =new Serie("Star Gate",11,"Ciencia Ficción","Syfy");
        Serie starWars =new Serie("Star Wars",9,"Ciencia Ficción","LucasFilm");
        Serie lucifer =new Serie("Lucifer",5,"Ciencia Ficción","Netflix");
        Serie flash =new Serie("Flash",6,"Ciencia Ficción","The CW");


        List<Serie>series  = Arrays.asList(dark,flash,lucifer,starGate,starWars);

        VideoJuego halo = new VideoJuego("Halo CE", 6,"Acción","Bungie Studios");
        VideoJuego ageOfEmpires = new VideoJuego("Age Of Empires", 5,"Estrategia","Microsoft");
        VideoJuego ageOfMithology = new VideoJuego("Age Of Mithology", 6,"Estrategia"," Ensemble Studios");
        VideoJuego lol = new VideoJuego("League Of Legends", 21,"Estrategia","Riot Games");
        VideoJuego creativeDestruction = new VideoJuego("Creative Destruction", 20,"Acción","NetEase");

        List<VideoJuego>videoJuegos  = Arrays.asList(ageOfEmpires,ageOfMithology,creativeDestruction,halo,lol);

        Serie serieTemporal= new Serie();
        VideoJuego videoJuegoTemporal= new VideoJuego();

        for (int i = 0; i <series.size(); i++) {
            if (series.get(i).getNumeroDeTemporadas()>serieTemporal.getNumeroDeTemporadas()){
                serieTemporal=series.get(i);
            }
        }
        System.out.println(serieTemporal.toString());
        System.out.println("\n");

        for (int i = 0; i <videoJuegos.size(); i++) {
            if (videoJuegos.get(i).getHorasEstimadas()>videoJuegoTemporal.getHorasEstimadas()){
                videoJuegoTemporal=videoJuegos.get(i);
            }
        }
        System.out.println( videoJuegoTemporal.toString());
    }
}
