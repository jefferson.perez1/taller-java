package persona;

import java.util.Random;

public class DNI {

    private int randomNumero;
    private String[] letra ={"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M","N","O","P","Q","R","S","T","U","V","W", "X","Y","Z" };


    public int numeros() {
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(90000000);
        return n;
    }
    public String letra(){
        int numRandon = (int) Math.round(Math.random() * 26 ) ;

        return getLetra()[numRandon];
    }

    public String dni(){
        return numeros()+""+letra();
    }

    public String[] getLetra() {
        return letra;
    }

}
