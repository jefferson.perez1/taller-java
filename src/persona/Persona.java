package persona;

public class Persona {

    private String nombre ;
    private int edad = 0 ;
    private DNI dni = new DNI();
    private char sexo ;
    private double peso =0;
    private double altura =0;


    public Persona(){

    }

    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
    }

    public Persona(String nombre, int edad, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
    }



    public int calcularIMC(){
        double pesoIdeal = getPeso()/Math.pow(getAltura(),2);

        if(pesoIdeal<20){
            return -1;
        }
        else if (pesoIdeal >= 20 && pesoIdeal <=25){
            return 0;
        }
        else
            return 1;
    }

    public String imprimirIMC(){
        if (calcularIMC()==-1){
            return   "Peso Ideal";
        }if (calcularIMC()==0){
            return   "Está por debajo de su peso ideal";
        }else
           return  "Tiene sobre peso";
    }


    public boolean esMayorDeEdad(){
        if (getEdad()>=18) {
            return true;
        }
        else
            return false;
    }
    public String imprimirEdad(){
        if (esMayorDeEdad()){
            return " Es mayor de edad";
        }else return " Es menor de edad";
    }

    public void comprobarSexo(char sexo){
        sexo =getSexo();
        if (sexo==('h')){
            setSexo(sexo);
        }else if (sexo=='m'){
            setSexo('m');
        }else setSexo('h');
    }

    public String dni(){
        return dni.dni();
    }

@Override
    public String toString(){
        comprobarSexo(getSexo());
     return " Nombre: " + getNombre()+"\n" + " Edad: " + getEdad()+" "+ imprimirEdad()+"\n" + " DNI: " +
             dni()+"\n"+" Sexo: "+getSexo()+"\n"+" Peso: "+getPeso()+"\n"+" Altura: "+getAltura()+"\n"+" IMC : "+imprimirIMC();

           }
           public void imprimirToString(){
               System.out.println(toString());
           }




    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public DNI getDNI() {
        return dni;
    }


    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
