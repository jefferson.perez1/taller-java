package electrodomesticos;

public class Electrodomestico {

    double  precioBase = 100;
    String color = "blanco";
    char consumoEnergetico = 'F';
    double peso = 5;


    public Electrodomestico() {
    }


    public Electrodomestico(double precioBase, double peso) {
        this.precioBase = precioBase;
        this.peso = peso;
    }

    public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
        this.precioBase = precioBase;
        this.color = color;
        this.consumoEnergetico = consumoEnergetico;
        this.peso = peso;
    }


    public void comprobarConsumoEnergetico(char letra){
        if (letra =='A' ||letra =='B' ||letra =='C' ||letra =='D' ||letra =='E' ||letra =='F') {
            this.consumoEnergetico = letra;
        }

        System.out.println(getConsumoEnergetico());
    }

    public void comprobarColor(String color){
        if (color.equalsIgnoreCase("negro")||color.equalsIgnoreCase("rojo")||color.equalsIgnoreCase("gris")||color.equalsIgnoreCase("azul")|| color.equalsIgnoreCase("blanco")){
            this.color=color;
            System.out.println(getColor());
        }else {
            this.color = "blanco";
            System.out.println(getColor());
        }
    }



    public double precioFinal(){

        if (consumoEnergetico=='A'){
            this.precioBase =+ 100;
        }else if (consumoEnergetico=='B'){
            this.precioBase += 80;
        }else  if (consumoEnergetico=='C'){
            this.precioBase += 60;
        }else if (consumoEnergetico=='D'){
            this.precioBase += 50;
        }else  if (consumoEnergetico=='E'){
            this.precioBase += 30;
        }else if (consumoEnergetico=='F'){
            this.precioBase += 10;
        }else if (peso>=0 && peso<=19){
            this.precioBase += 10;
        }else if (peso>=20 && peso<=49){
            this.precioBase += 50;
        }else if (peso>=50 && peso<=79){
            this.precioBase += 80;
        }else if (peso>=80){
            this.precioBase += 100;
        }

         return this.precioBase;

    }









    public double getPrecioBase() {
        return precioBase;
    }

    public String getColor() {
        return color;
    }

    public char getConsumoEnergetico() {
        return consumoEnergetico;
    }

    public double getPeso() {
        return peso;
    }
}
