package electrodomesticos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Electrodomestico lavadoraSamsung = new Lavadora(500, "azul", 'C', 50, 41);
        Electrodomestico lavadoraWirepool = new Lavadora(700, "negra", 'C', 50, 41);
        Electrodomestico lavadoraCentrales = new Lavadora(900, "verde", 'C', 50, 41);
        Electrodomestico lavadoraHP = new Lavadora(900, "verde", 'C', 50, 41);
        Electrodomestico lavadoraTop = new Lavadora(900, "verde", 'C', 50, 41);

        Electrodomestico televisorSamsung = new Television(50, "azul", 'F', 50, 41, true);
        Electrodomestico televisorhuawei = new Television(50, "azul", 'F', 50, 41, true);
        Electrodomestico televisorDell = new Television(50, "azul", 'F', 50, 41, true);
        Electrodomestico televisorApple = new Television(50, "azul", 'F', 50, 41, true);
        Electrodomestico televisor = new Television(50, "azul", 'F', 50, 41, true);

        List<Electrodomestico> electrodomesticos = Arrays.asList(lavadoraSamsung, lavadoraCentrales, lavadoraWirepool, lavadoraHP, lavadoraTop,
                televisor, televisorApple, televisorDell, televisorhuawei, televisorSamsung);
        double suma=0;
        for (Electrodomestico electrodomestico : electrodomesticos) {

            if (electrodomestico instanceof Lavadora) {
                suma+=electrodomestico.precioFinal();
            }


        }System.out.println("El precio final de todas las lavadoras es: " + suma);
        for (Electrodomestico electrodomestico : electrodomesticos) {
            if (electrodomestico instanceof Television) {
                suma+=electrodomestico.precioFinal();

            }
        } System.out.println("El precio final de todos los televisores es: " + suma);
        for (Electrodomestico electrodomestico : electrodomesticos) {
            suma+=electrodomestico.precioFinal();
        }
        System.out.println("El precio final de todos los electrodomesticos es : " + suma);
    }
}
