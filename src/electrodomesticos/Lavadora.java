package electrodomesticos;

public class Lavadora  extends  Electrodomestico{

    private double carga= 5;

    public Lavadora() {
    }

    public Lavadora(double precioBase, double peso) {
        super(precioBase, peso);
    }

    public Lavadora(double precioBase, String color, char consumoEnergetico, double peso, double carga) {
        super(precioBase, color, consumoEnergetico, peso);
        this.carga = carga;
    }

    @Override
    public double precioFinal(){
        super.precioFinal();
        if (getCarga() > 30) {
            precioBase +=50;
        }
        return getPrecioBase();
    }


    public double getCarga() {
        return carga;
    }
}
