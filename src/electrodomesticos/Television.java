package electrodomesticos;

public class Television extends Electrodomestico {

    private double resolucion = 20;
    private boolean sintonizadorTDT = false;


    public Television() {
    }

    public Television(double precioBase, double peso) {
        super(precioBase, peso);
    }

    public Television(double precioBase, String color, char consumoEnergetico, double peso, double resolucion, boolean sintonizadorTDT) {
        super(precioBase, color, consumoEnergetico, peso);
        this.resolucion = resolucion;
        this.sintonizadorTDT = sintonizadorTDT;
    }


    @Override
    public double precioFinal() {
        super.precioFinal();
        if (this.resolucion>40){
            double aumento= precioBase*0.3;
            precioBase+=aumento;
        }if(sintonizadorTDT != false){
            precioBase+=50;
        }
        return getPrecioBase();
    }

    public double getResolucion() {
        return resolucion;
    }

    public boolean isSintonizadorTDT() {
        return sintonizadorTDT;
    }
}
