package contarLetras;

import java.util.Scanner;

public class Contar {

    private String frase;

    public void bienvenida(){
        System.out.println("Ingrese la frase para verificar la longitud y cuantas vocales tiene:");
    }
        public void entrada() {
            Scanner sc = new Scanner(System.in);
         setFrase(sc.nextLine());
        }




    public void contador(){

        System.out.println("El tamaño de la frase es de: " + getFrase().length()+" Con espacios incluidos");
    }

    public void contadorDeVocales(){
        int vocalA = 0;
        int vocalE = 0;
        int vocalI = 0;
        int vocalO = 0;
        int vocalU = 0;


        for (int i = 0; i < getFrase().length(); i++) {



            if (getFrase().toUpperCase().charAt(i) == 'A') {
                vocalA++;
            } if (getFrase().toUpperCase().charAt(i) == 'E') {
                vocalE++;
            } if (getFrase().toUpperCase().charAt(i) == 'I') {
                vocalI++;
            }  if (getFrase().toUpperCase().charAt(i) == 'O') {
                vocalO++;
            }  if (getFrase().toUpperCase().charAt(i) == 'U') {
                vocalU++;
            }
        }

        System.out.println("Cantidad de vocales A es de: " + vocalA);
        System.out.println("Cantidad de vocales E es de: " + vocalE);
        System.out.println("Cantidad de vocales I es de: " + vocalI);
        System.out.println("Cantidad de vocales O es de: " + vocalO);
        System.out.println("Cantidad de vocales U es de: " + vocalU);
    }

    public void infoGeneralDeLaFrase(){
        bienvenida();
        entrada();
        contador();
        contadorDeVocales();
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }
}
