package calcularIVA;

import java.util.Scanner;

public class Iva {
    private Double precioProducto;
    private final Double IVA= 0.21;

    public void ingresoPrecioProducto(){
        System.out.print(" Ingrese el precio del producto: ");
        Scanner precioProducto = new Scanner(System.in);
        setPrecioProducto(precioProducto.nextDouble());
    }
    public double operacionIVA(){
        ingresoPrecioProducto();
        Double ivaProducto=getPrecioProducto()*getIVA();
        Double nuevoPrecio= getPrecioProducto()+ivaProducto;
        return nuevoPrecio;
    }
    public void calcularNuevoPrecio(){
        System.out.println(" El precio del producto mas el IVA del 21% Es: "+operacionIVA());
    }

    public Double getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(Double precioProducto) {
        this.precioProducto = precioProducto;
    }

    public Double getIVA() {
        return IVA;
    }
}
