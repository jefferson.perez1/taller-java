package comparador;

import java.util.Scanner;

public  class Variable {

    private Double numeroUno;
    private Double numeroDos;



    public String informacionNumeroMayor(){
        String mensaje = " Es Mayor que ";
        return  mensaje;
    }
    public void informacionDeIgualdad(){
        System.out.println( "Los numeros son iguales ");
    }
    public void informacionGeneral(){
       System.out.println("Se compararán dos numeros racionales, Por favor ingresar dichos numeros a continuación: ");

    }



    public void ingresoNumeros(){
        Scanner entradaNumero1 = new Scanner(System.in);
        setNumeroUno(entradaNumero1.nextDouble());
        Scanner entradaNumero2 = new Scanner(System.in);
        setNumeroDos(entradaNumero2.nextDouble());
    }

    public void comparadorDeNumeros(){
        informacionGeneral();

        ingresoNumeros();

        if(getNumeroUno()>getNumeroDos()){
            System.out.println( getNumeroUno() + informacionNumeroMayor() + getNumeroDos());
        }
        else if ((getNumeroDos()>getNumeroUno())){
            System.out.println(getNumeroDos() + informacionNumeroMayor() + getNumeroUno());
        }
       else  informacionDeIgualdad();
    }


    public Double getNumeroUno() {
        return numeroUno;
    }

    public void setNumeroUno(Double numeroUno) {
        this.numeroUno = numeroUno;
    }

    public Double getNumeroDos() {
        return numeroDos;
    }

    public void setNumeroDos(Double numeroDos) {
        this.numeroDos = numeroDos;
    }
}
